<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'WP');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[Y4N(rb9>ZEk=#9R5H$*Ik+3r,U+y(vk( 3t{K)9Lm;&sGnc?YJF9>dN)K^(J|mU');
define('SECURE_AUTH_KEY',  'TfvY+EO~G4hhWhy&f^@E)oqGsD5}iO: <.p%L6_,)eB4m9VC0K`7-|T7mBH}P3pR');
define('LOGGED_IN_KEY',    'SmM4V`N!QJKhc%S*&gNRN#+ixHSo86Ge|W10|y^To!kM(+O`NuQJRHYU^eg@]*6?');
define('NONCE_KEY',        'g&n9be+fS=0-<?.<1I{_>V,!XSD}-:c%p!X+OaV^hz2HOpo[7-3[=*@>m0pFwX=y');
define('AUTH_SALT',        'U.c}/-hn6<+m$GH$:`|JcUI4<DaxH=NJB-W3D3E6cl=PX|$XL={wR+$ev%_1B:Cb');
define('SECURE_AUTH_SALT', 'E_4.<8J.upnbwQ_uwStg)bRLP^AqpGc,~K6D]-S`Z+$<Oug|`uuM0~+/,|iD]Za-');
define('LOGGED_IN_SALT',   'U[U{`XL#*L%5!A^qt!FTW5qotoWP|ldb7`,NqJsT+1@9SSj3wN-mNVMFE8_?-)f>');
define('NONCE_SALT',       '<9nBsZ|*jx@(FFVJ(yXb)tPvouBao):jQEF*-bpH/PINGHZv:Ur|3SxhQRg+B[lo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/** To log error to debug.log */
define( 'WP_DEBUG_LOG', true );

/** Display debug message within HTML */
define( 'WP_DEBUG_DISPLAY', true );

define('WP_ALLOW_MULTISITE', true);

define( 'SAVEQUERIES', true );



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
