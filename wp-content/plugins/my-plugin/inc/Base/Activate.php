<?php

/**
 * @package MyPlugin
 */

namespace Inc\Base;


/**
 * Class Activate
 * @package Inc\Base
 */
class Activate
{
	public static function activate() {
		flush_rewrite_rules();
	}
}