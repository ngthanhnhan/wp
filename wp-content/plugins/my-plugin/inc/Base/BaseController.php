<?php
/**
 * Created by PhpStorm.
 * User: Nhan
 * Date: 9/25/2018
 * Time: 9:51 AM
 */

namespace Inc\Base;

class BaseController {
	public $plugin_path;
	public $plugin_url;
	public $plugin_basename;

	function __construct() {
		$this->plugin_path     = plugin_dir_path( dirname( __FILE__, 2 ) );
		$this->plugin_url      = plugin_dir_url( dirname( __FILE__, 2 ) );
		$this->plugin_basename = plugin_basename( dirname( __FILE__, 3 ) ) . '/my-plugin.php';
	}
}
