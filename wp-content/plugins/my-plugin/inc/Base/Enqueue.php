<?php
/**
 * Created by PhpStorm.
 * User: Nhan
 * Date: 9/25/2018
 * Time: 1:08 AM
 */

namespace Inc\Base;

class Enqueue extends BaseController {

	public function register() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	function enqueue() {
		// Enqueue all scripts file
		wp_enqueue_style( 'mypluginstyle', $this->plugin_url . '/assets/mystyle.css',
			[], '1.0.0', false );
		wp_enqueue_script( 'mypluginscript', $this->plugin_url . '/assets/myscript.js',
			[], '1.0.0', false );
	}
}