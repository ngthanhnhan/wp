<?php

/**
 * @package MyPlugin
 */

namespace Inc\Base;

use Inc\Base\BaseController;

/**
 * Class Activate
 * @package Inc\Base
 */
class SettingsLink extends BaseController{

	public function register() {
		add_filter( "plugin_action_links_" . $this->plugin_basename, array( $this, 'settings_link' ) );
	}

	public function settings_link( $links ) {
		// Add custom setting link
		$settings_link = '<a href="admin.php?page=my_plugin">Settings</a>';
		array_push( $links, $settings_link );
		return $links;
	}
}