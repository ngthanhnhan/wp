<?php

/**
 * @package MyPlugin
 */

namespace Inc;

final class Init {
	/**
	 * Store all classes inside an array
	 * @return array Full of classes
	 */
	public static function get_services() {
		return [
			Pages\Admin::class,
			Base\Enqueue::class,
			Base\SettingsLink::class
		];
	}

	/**
	 * Loop through the classes, initialize them,
	 * then call register function if it exists
	 */
	public static function register_services() {
		foreach ( self::get_services() as $class ) {
			$service = self::instantiate( $class );
			if ( method_exists( $service, 'register' ) ) {
				$service->register();
			}
		}
	}

	/**
	 * Initialize the class instance
	 *
	 * @param $class class from service array
	 *
	 * @return mixed instance of class
	 */
	private static function instantiate( $class ) {
		$service = new $class;

		return $service;
	}
}


//use Inc\Base\Activate;
//use Inc\Base\Deactivate;
//use Inc\Pages\Admin;
//
//if ( ! class_exists( 'MyPlugin' ) ) {
//
//	class MyPlugin {
//
//		public $plugin;
//
//		function __construct() {
//			$this->plugin = plugin_basename( __FILE__ );
//		}
//
//		function register() {
//			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
//
//			add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
//
//			add_filter( "plugin_action_links_" . $this->plugin, array( $this, 'settings_link' ) );
//
//		}
//
//		public function settings_link( $links ) {
//			// Add custom setting link
//			$settings_link = [ '<a href="admin.php?page=my_plugin">Settings</a>' ];
//			array_merge( $links, $settings_link );
//			return $links;
//		}
//
//		public function add_admin_pages() {
//			add_menu_page( 'My Plugin', 'MP', 'manage_options', 'my_plugin',
//				array( $this, 'admin_index' ), 'dashicons-store', 110 );
//		}
//
//		public function admin_index() {
//			require_once plugin_dir_path( __FILE__ ) . 'templates/admin.php';
//		}
//
//		function activate() {
//			Activate::activate();
//		}
//
//		function deactivate() {
//			Deactivate::deactivate();
//		}
//
//		function uninstall() {
//
//		}
//
//		protected function create_post_type() {
//			add_action( 'init', array( $this, 'custom_post_type' ) );
//		}
//
//		protected function custom_post_type() {
//			register_post_type( 'book', [ 'public' => true, 'label' => 'Books' ] );
//		}
//
//		function enqueue() {
//			// Enqueue all scripts file
//			wp_enqueue_style( 'mypluginstyle', plugins_url( '/assets/mystyle.css', __FILE__ ),
//				[], '1.0.0', false );
//			wp_enqueue_script( 'mypluginscript', plugins_url( '/assets/myscript.js', __FILE__ ),
//				[], '1.0.0', false );
//		}
//
//	}
//
//	$myPlugin = new MyPlugin();
//	$myPlugin->register();
//
//	// activation
//	register_activation_hook( __FILE__, array( $myPlugin, 'activate' ) );
//
//	// deactivation
//	register_deactivation_hook( __FILE__, array( $myPlugin, 'deactivate' ) );
//
//	// uninstall
//
//}