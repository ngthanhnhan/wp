<?php
/**
 * @package MyPlugin
 */
/*
Plugin Name: My Plugin
Plugin URL: http://nhan.nguyen.me
Description: This is my plugin
Version: 1.0.0
Author: Nhan Nguyen
Author URI: http://nhan.nguyen.no
License: GPLv2 or later
Text Domain: my-plugin
*/

defined( 'ABSPATH' ) or die ( 'Hey, you can\'t do that, you silly man!' );

if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * Register all service in services array
 */
if ( class_exists( 'Inc\\Init' ) ) {
	Inc\Init::register_services();
}

/**
 * The code that runs during plugin activation
 */
function activate_myplugin() {
	\Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_myplugin' );

/**
 * The code that run during plugin deactivation
 */
function deactivate_myplugin() {
	\Inc\Base\Deactivate::deactivate();
}
register_deactivation_hook( __FILE__, 'deactivate_myplugin' );

































