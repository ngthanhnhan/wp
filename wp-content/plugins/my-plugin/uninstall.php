<?php


/**
 * Trigger this file on Plugin uninstall
 *
 * @package MyPlugin
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	die;
}

// Clear database
$books = get_posts( array( 'post_type' => 'books', 'numberposts' => -1 ) );

foreach ( $books as $book ) {
	wp_delete_post( $book->ID, true );
}